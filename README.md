# A small Weather Application

## CDNs  

1. Google Map CDN
    - Needed to show the map
    - You must provide your own google map api key (or ask me mine)
    - In case you can't actually use the map for any reason a video is attached to the projet showing how it works
2. TailwindCSS
    - Makes CSS way easier when we actually don't like doing CSS

## Script
`app.js` contains the complete application code.
`Forecast` is the class that manage the application, you can create a new `Forecast` by clicking on the map or searching a city through the menu bar.

You can click on the different days at the bottom of the application to show each day forecast (only the first day is provided with full details since this is how the API works)

## CSS

Thanks to tailwind the CSS is rather small, it only imports the material icons font in the project and has a class for the currently selected forecast.

## Inspiration

I'm not a great designer I saw different pictures on Dribbble and used them as a reference (but everything is handmade since dribbble only provides 1 picture per shot, no code or anything else)

### Author  
👷🏻‍♂️Axel Moriceau