let mapVisible = false
let forecast = null

class Forecast {

    city = null;
    coords = null
    currentDay = 0
    forecastData = null

    // Map API weather conditions to Google Icons Equivalents ('cause the API icons are hideous)
    static conditions = {
        "averses-de-neige-faible": "snowing",
        "averses-de-pluie-faible": "rainy",
        "averses-de-pluie-forte": "rainy",
        "averses-de-pluie-moderee": "rainy",
        "brouillard": "foggy",
        "ciel-voile": "cloud",
        "couvert-avec-averses": "rainy",
        "developpement-nuageux": "partly_cloudy_day",
        "eclaircies": "partly_cloudy_day",
        "ensoleille": "sunny",
        "faibles-passages-nuageux": "partly_cloudy_day",
        "faiblement-nuageux": "partly_cloudy_day",
        "faiblement-orageux": "electric_bolt",
        "fortement-nuageux": "cloud",
        "fortement-orageux": "thunderstorm",
        "gel": "severe_cold",
        "neige-faible": "snowing",
        "neige-forte": "weather_snowy",
        "neige-moderee": "sunny_snowing",
        "nuit-avec-averses": "rainy",
        "nuit-avec-averses-de-neige-faible": "snowing",
        "nuit-avec-developpement-nuageux": "nights_stay",
        "nuit-bien-degagee": "clear_night",
        "nuit-claire": "clear_night",
        "nuit-claire-et-stratus": "clear_night",
        "nuit-faiblement-orageuse": "nights_stay",
        "nuit-legerement-voilee": "nights_stay",
        "nuit-nuageuse": "nights_stay",
        "orage-modere": "electric_bolt",
        "pluie-et-neige-melee-faible": "snowing",
        "pluie-et-neige-melee-forte": "weather_snowy",
        "pluie-et-neige-melee-moderee": "weather_snowy",
        "pluie-faible": "rainy",
        "pluie-forte": "rainy",
        "pluie-moderee": "rainy",
        "stratus": "foggy",
        "stratus-se-dissipant": "foggy"
    }

    constructor({city, coordinates = null}, empty = false) {
        if (empty) {
            this.buildEmptyForecast()
            return
        }

        let result = null
        if (coordinates) {
            this.coords = coordinates
            result = (async () => await this.#getForecastByCoordinate(coordinates))()
        } else {
            this.city = city
            result = (async () => await this.#getForecastByName(city))()
        }

        result.then(data => {
            if (data.errors) {
                this.buildEmptyForecast()
                return
            }
            this.forecastData = data
            this.buildGlobalForecast(data)
        })

    }

    #buildCurrentForecast(forecast, day) {
        const todayForecast = document.getElementById('today-forecast')
        todayForecast.innerHTML = ""

        let temperature = Math.abs(+(forecast?.tmp ?? forecast?.tmax)).toString()
        if (+(forecast?.tmp ?? forecast?.tmax) < 0) {
            temperature = `<span class="font-extralight">-</span>${temperature}`
        }

        let place = this.city ? `À  ${this.city}` : this.coords ? `${this.coords.lat.toFixed(10)},${this.coords.long.toFixed(10)}` : ''

        const todayForecastHtml = `
            <div class="py-36 flex items-center justify-center relative">
                <div class="font-bold text-10xl z-10">${temperature}<span class="font-extralight">°</span></div>
                <div class="inline-block pl-4 space-y-4 z-10">
                    <div class="text-lg">${forecast?.hour ?? forecast?.date ?? ''}</div>
                    <div class="font-semibold text-6xl">${day}</div>
                    <div class="text-lg">${forecast?.condition}</div>
                    <div class="font-semibold text-2xl underline">${place}</div>
                </div>
                <div class="absolute z-0 inset-0 m-auto w-fit h-fit text-gray-400 opacity-25">
                    <span class="current-icon material-symbols-outlined text-20xl ">
                        ${this.#loadIcon(forecast?.condition_key, +forecast?.tmp ?? 0)}
                    </span>
            </div>
        `

        todayForecast.appendChild(new DOMParser().parseFromString(todayForecastHtml, "text/html").body.firstChild)
    }

    #buildWeekForecasts(forecasts) {
        const weekElement = document.getElementById('week-forecast')
        weekElement.innerHTML = ''

        const weekForecastHtml = () => {
            let dashtml = `<div class="grid-span-1 flex items-center w-full px-8"><div class="h-[1px] w-full bg-black"></div></div>`
            let html = dashtml
            forecasts.forEach((dailyForecast, index) => {
                html += `<div data-day="${index}" id="day-${index}" class="grid-span-1 px-10 pointer-cursor weekDayForecast">
                            <div class="flex flex-col item-center justify-center py-2 ${this.currentDay === index ? 'current' : ''}">
                                <div class="w-full h-fit text-black text-center">
                                    <span class="daily-icon material-symbols-outlined text-md">${this.#loadIcon(dailyForecast.condition_key, +dailyForecast.tmin)}</span>
                                </div>
                                <span class="text-center">${dailyForecast.day_short}</span>
                                <span class="text-center">${dailyForecast.tmax}°</span>
                                <span class="text-center opacity-50">${dailyForecast.tmin}°</span>
                            </div>
                        </div>`
            })
            return html + dashtml
        }
        weekElement.append(...(new DOMParser().parseFromString(weekForecastHtml(), "text/html").body.childNodes))

        const days = [...document.getElementsByClassName('weekDayForecast')]
        days.forEach(day => day.addEventListener('click', () => {
            this.currentDay = +day.dataset.day

            this.buildGlobalForecast(this.forecastData)
        }))
    }

    #buildCurrentDayHourlyForecast({hourly_data}) {

        const dataset = Object.values(hourly_data)
        document.getElementById('hourly-forecast').innerHTML = ""

        let hourlyForecastHtml = ''
        dataset.forEach((hour, index) => {
            hourlyForecastHtml += `<li class="flex items-center justify-center w-full user-select-none cursor-default" title="${hour.CONDITION}">
                        <span class="tracking-wider">${('00' + index).slice(-2)}:00</span>
                        <span class="daily-icon material-symbols-outlined mx-5">${this.#loadIcon(hour.CONDITION_KEY, hour.TMP2m ?? 0)}</span>
                        <span class="font-bold tracking-wider">${('0000' + ((+hour?.TMP2m ?? 0).toFixed(1))).slice(-4)}°</span>
                    </li>`
        })

        document.getElementById('hourly-forecast').append(...(new DOMParser().parseFromString(hourlyForecastHtml, "text/html").body.childNodes))
    }

    buildGlobalForecast(forecast) {
        const forecastsKeys = ['fcst_day_0', 'fcst_day_1', 'fcst_day_2', 'fcst_day_3', 'fcst_day_4']
        const forecastDate = document.getElementById('forecast-date')

        forecastDate.innerHTML = new Intl.DateTimeFormat(['fr', 'en']).format(new Date())

        this.#buildCurrentForecast(
            this.currentDay === 0 ? forecast?.current_condition : forecast[forecastsKeys[this.currentDay]],
            this.currentDay === 0 ? forecast?.fcst_day_0.day_long : forecast[forecastsKeys[this.currentDay]].day_long
        )
        this.#buildWeekForecasts([forecast.fcst_day_0, forecast.fcst_day_1, forecast.fcst_day_2, forecast.fcst_day_3, forecast.fcst_day_4])
        this.#buildCurrentDayHourlyForecast(forecast[forecastsKeys[this.currentDay]])
    }

    buildEmptyForecast() {
        this.#buildCurrentForecast({
            tmp: -10,
            hour: '00:00',
            condition: 'inconnue',
            condition_key: 'inconnue'
        }, 'Bouh c\'est froid, je ne connais pas cette ville!')

        const weekElement = document.getElementById('week-forecast')
        weekElement.innerHTML = ''
        document.getElementById('hourly-forecast').innerHTML = ""
    }

    async #getForecastByCoordinate({lat, long}) {
        return await fetch(`https://prevision-meteo.ch/services/json/lat=${lat}lng=${long}`)
            .then(async (response) => await response.json())
    }

    async #getForecastByName(city) {
        return await fetch(`https://prevision-meteo.ch/services/json/${city}`)
            .then(async (response) => await response.json())
    }

    #loadIcon(condition = 'ensoleillé', temperature) {
        if (temperature < -5) {
            condition = 'gel'
        }
        return Forecast.conditions[condition]
    }
}


document.getElementById('city').addEventListener('change', async () => {
    forecast = new Forecast({city: document.getElementById('city').value})
})
document.getElementById('city').addEventListener('keyup', async (event) => {
    (event.code === "Enter") && (forecast = new Forecast({city: document.getElementById('city').value}))
})
document.getElementById('openMap').addEventListener('click', () => {
    switchMapState()
})

function switchMapState() {
    if (mapVisible) {
        document.getElementById('map').classList.add('hidden')
        mapVisible = false
        return
    }

    document.getElementById('map').classList.remove('hidden')
    mapVisible = true
}

// GoogleMaps required fallback - This is the function given in the documentation
// The only modified part is that we don't need to show a box over the map that displays the coordinates
// we use them in our forecast instead (inside the listener)
function initMap() {
    const coords = {lat: 44.83707456462219, lng: -0.5831439337594935};

    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 4,
        center: coords,
    });

    // Create the initial InfoWindow.
    let infoWindow = new google.maps.InfoWindow({
        content: "Cliquez sur la carte pour voir la météo à cette endroit.",
        position: coords,
    });

    infoWindow.open(map);

    // Configure the click listener.
    map.addListener("click", (mapsMouseEvent) => {
        // Close the current InfoWindow.
        infoWindow.close();

        let curCoords = mapsMouseEvent.latLng.toJSON()
        forecast = new Forecast({coordinates: {lat: curCoords.lat, long: curCoords.lng}})
        document.getElementById('city').value = null
        setTimeout(switchMapState, 500)
    });
}

window.initMap = initMap

function init() {
    new Forecast({}, true)
    document.getElementById('map').classList.add('hidden')
}

init()